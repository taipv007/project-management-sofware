from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.template import loader
from django.views.generic import (CreateView, DetailView, ListView, UpdateView, DeleteView)
from django.urls import reverse
from .models import Project, Task, User, Phase, Common_User_Project, Common_User_Task, Feedback, File, QA
from .forms import LoginForm, ProjectForm, TaskForm, PhaseForm

def index(request, user_id):
    usr = User.objects.get(pk=user_id)
    return render(request, 'ProjectManagement/index.html',{'user':usr})

def login(request):
    if request.method == 'POST': 
        acc = request.POST.get('account')
        psw = request.POST.get('password')
        if User.objects.filter(Account=acc) :
            idc = User.objects.get(Account=acc) 
            if idc.Password == psw :       
                return HttpResponseRedirect(reverse('ProjectManagement:index',args=(idc.id,)))
            return HttpResponse("Wrong Password !!! ")
        return HttpResponse("Your Account Is Not Existed !!! ")
    form = LoginForm() 
    return render(request, 'ProjectManagement/login.html')


# def detail(request, project_id):
#     try:
#         project = Project.objects.get(pk=project_id)
#     except Project.DoesNotExist:
#         raise Http404("Question does not exist")
#     return render(request, 'ProjectManagement/detail.html', {'project': project})

def tasks(request, user_id, project_id, task_id):
    latest_phase_list = Phase.objects.order_by('-pk')
    usr = User.objects.get(pk=user_id)
    tsk = Task.objects.get(pk=task_id)
    prj = Project.objects.get(pk=project_id)
    context = {'latest_phase_list': latest_phase_list, 'user':usr, 'task':tsk, 'project': prj}
    return render(request, 'ProjectManagement/tasks.html', context)

def activity(request):
    return render(request, "ProjectManagement/activity.html")

def projectoverview(request, user_id):
    latest_project_list = Project.objects.all()
    usr = User.objects.get(pk=user_id)
    context = {'latest_project_list': latest_project_list, 'user':usr}
    return render(request, 'ProjectManagement/projectoverview.html', context)

def newproject(request, user_id):
    usr = User.objects.get(pk=user_id)
    newpj = Project()
    if request.method == 'POST': 
        form = ProjectForm(request.POST)
        if form.is_valid():
            newpj.Name = request.POST['Name']
            newpj.Description = request.POST['Description']
            newpj.StartDate = request.POST['StartDate']
            newpj.DueDate = request.POST['DueDate']
            newpj.Done = request.POST['Done']
            newpj.Creator = usr
            newrl = Common_User_Project() 
            newrl.User = usr
            if not Project.objects.filter(Name=newpj.Name) and newpj.DueDate > newpj.StartDate:
                newpj.save() 
                newrl.Project = newpj
                newrl.save()
                response = HttpResponse()
                response.write("Your project: <b>" + request.POST['Name'] + "</b> has been created succesfully !!! </br>")
                response.write("<a href=\"projectoverview\"> Back to Project Overview </a> ")
                return response 
            # raise ProjectForm.ValidationError("Project existed or Time is invalid ! ")
            response = HttpResponse()
            response.write("<h1>You've tried to create a new project !</h1></br>")
            response.write("Your project: <b>" + request.POST['Name'] + "</b> has been existed or Dates are Invalid !!! </br>")
            response.write("Please back to previous page and check the infomations !")
            return response 
        return HttpResponseRedirect(reverse('ProjectManagement:projectoverview'))
    form = ProjectForm()
    return render(request, 'ProjectManagement/newproject.html', {'form' : form, 'user':usr}) 


def projects(request, project_id, user_id):
    try:
        project = Project.objects.get(pk=project_id)
        usr = User.objects.get(pk=user_id)
    except Project.DoesNotExist:
        raise Http404("Project does not exist")
    return render(request, 'ProjectManagement/projects.html', {'project': project, 'user':usr})

def editproject(request, project_id, user_id):
    # try:
    #     project = Project.objects.get(pk=project_id)
    # except Project.DoesNotExist:
    #     raise Http404("Project does not exist")
    # return render(request, 'ProjectManagement/editproject.html', {'project': project})
    usr = User.objects.get(pk=user_id)
    project = Project.objects.get(pk=project_id)
    if request.method == 'POST' : 
        form = ProjectForm(request.POST)
        # if form.is_valid():
        # project.Name = request.POST['Name']
        project.Description = request.POST['Description']
        project.StartDate = request.POST['StartDate']
        project.DueDate = request.POST.get('DueDate')
        project.Done = request.POST['Done']
        if (not Project.objects.filter(Name=request.POST['Name']) or project.Name == request.POST['Name'] ) and project.DueDate > project.StartDate:
            project.Name = request.POST['Name']
            project.save() 
            response = HttpResponse()
            response.write("Your project: <b>" + request.POST['Name'] + "</b> has been edited succesfully !!! </br>")
            response.write("<a href=\".\"> Back to Project Overview </a> ")
            return response 
        response = HttpResponse()
        response.write("<h1>You've tried to create a new project !</h1></br>")
        response.write("Your project: <b>" + request.POST['Name'] + "</b> has been existed or Dates are Invalid !!! </br>")
        response.write("Please back to previous page check the informations !")
        return response 
        # response = HttpResponse()
        # response.write("Form Unvalid")
        # return response        
        # return render(request, 'ProjectManagement/editproject.html', {'project': project})
    form = ProjectForm()
    return render(request, 'ProjectManagement/editproject.html', {'form' : form, 'project': project, 'user':usr}) 

def newtask(request, user_id, project_id):
    project = Project.objects.get(pk=project_id)
    usr = User.objects.get(pk=user_id)
    if request.method == 'POST': 
        form = ProjectForm(request.POST)
        if form.is_valid():
            newtask = Task()
            newtask.Name = request.POST['Name']
            newtask.Description = request.POST['Description']
            newtask.StartDate = request.POST['StartDate']
            newtask.DueDate = request.POST['DueDate']
            newtask.Done = request.POST['Done']
            newtask.Project = project
            newtask.Status = request.POST['Status']
            newtask.Priority = request.POST.get('Priority',False)
            newtask.Assignee = User.objects.get(pk=1)
            if not Task.objects.filter(Name=newtask.Name) and newtask.DueDate > newtask.StartDate:
                newtask.save() 
                response = HttpResponse()
                response.write("Your task: <b>" + request.POST['Name'] + "</b> has been created succesfully !!! </br>")
                response.write("<a href=\".\"> Back to Project Overview </a> ")
                return response 
            # raise ProjectForm.ValidationError("Project existed or Time is invalid ! ")
            response = HttpResponse()
            response.write("<h1>You've tried to create a new task !</h1></br>")
            response.write("Your task: <b>" + request.POST['Name'] + "</b> has been existed or Dates are Invalid !!! </br>")
            response.write("Please back to previous page and check the infomations !")
            return response 
        return render(request, 'ProjectManagement/newtask.html', {'user':usr ,'project': project})
    form = ProjectForm()
    return render(request, 'ProjectManagement/newtask.html', {'form' : form, 'project':project, 'user': usr }) 

def edittask(request, user_id, project_id, task_id):
    project = Project.objects.get(pk=project_id)
    task = Task.objects.get(pk=task_id)
    usr = User.objects.get(pk=user_id)
    if request.method == 'POST' : 
        form = ProjectForm(request.POST)
        if form.is_valid():
            task.Name = request.POST['Name']
            task.Description = request.POST['Description']
            task.StartDate = request.POST['StartDate']
            task.DueDate = request.POST['DueDate']
            task.Done = request.POST['Done']
            task.Project = project
            task.Status = request.POST['Status']
            task.Priority = request.POST['Priority']
            task.Assignee = User.objects.get(pk=1)
            if (not Task.objects.filter(Name=request.POST['Name']) or task.Name == request.POST['Name'] ) and task.DueDate > task.StartDate:
                task.Name = request.POST['Name']
                task.save() 
                response = HttpResponse()
                response.write("Your task: <b>" + request.POST['Name'] + "</b> has been edited succesfully !!! </br>")
                response.write("<a href=\".\"> Back to Project Overview </a> ")
                return response 
            response = HttpResponse()
            response.write("<h1>You've tried to create a new project !</h1></br>")
            response.write("Your task: <b>" + request.POST['Name'] + "</b> has been existed or Dates are Invalid !!! </br>")
            response.write("Please back to previous page check the informations !")
            return response 
        # response = HttpResponse()
        # response.write("Form Unvalid")
        # return response        
        # return render(request, 'ProjectManagement/editproject.html', {'project': project})
        return render(request, 'ProjectManagement/edittask.html', {'project': project, 'task': task})
    form = TaskForm()
    return render(request, 'ProjectManagement/edittask.html', {'form' : form, 'project': project, 'task' : task, 'user':usr}) 

def adduser(request, user_id, project_id):
    return render(request, "ProjectManagement/adduser.html") 

def newphase(request):
    form = PhaseForm() 
    return render(request, "ProjectManagement/newphase.html", {'form' : form}) 

def news(request, user_id):
    return render(request, "ProjectManagement/news.html") 

def support(request, user_id):
    return render(request, "ProjectManagement/support.html") 

def gantt(request, user_id):
    return render(request, "ProjectManagement/gantt.html") 

def forgot_password(request):
    return render(request, "ProjectManagement/forgot_password.html") 


def detail(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    return render(request, 'ProjectManagement/detail.html', {'project': project})

# def tasks(request, project_id):
#     response = "You're looking at the tasks of project %s."
#     return HttpResponse(response % project_id)

def user(request, project_id):
    return HttpResponse("You're looking at users on project %s." % project_id)

def subindex(request):
    latest_project_list = Project.objects.order_by('-DueDate')[:5]
    context = {'latest_project_list': latest_project_list}
    return render(request, 'ProjectManagement/subindex.html', context)